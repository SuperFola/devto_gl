cmake_minimum_required(VERSION 3.8)

project(devto_gl)

# configuring compilation flags

if (NOT DEBUG)
    message(STATUS "To enable debug mode, use -DDEBUG=true")
    message(STATUS "Falling back to release")
    
    set(DEBUG "false" CACHE STRING "Enables debug mode" FORCE)
endif()

# searching for modules and libraries

list(APPEND CMAKE_MODULE_PATH "${devto_gl_SOURCE_DIR}/cmake")

find_package(OpenGL REQUIRED)
if (OPENGL_FOUND)
    message(STATUS "Packages: OpenGL found")
    include_directories(${OPENGL_INCLUDE_DIRS})
    link_libraries(${OPENGL_LIBRARIES})
endif()

if (UNIX OR LINUX)
    find_package(OpenAL REQUIRED)
    if (OPENAL_FOUND)
        message(STATUS "Packages: OpenAL found")
        include_directories(${OPENAL_INCLUDE_DIRS})
        link_libraries(${OPENAL_LIBRARIES})
    endif()

    find_package(GLFW REQUIRED)
    if (GLFW_FOUND)
        message(STATUS "Packages: GLFW found")
        include_directories(${GLFW_INCLUDE_DIRS})
        link_libraries(${GLFW_LIBRARIES})
    endif()

    find_package(GLM REQUIRED)
    if (GLM_FOUND)
        message(STATUS "Packages: GLM found")
        include_directories(${GLM_INCLUDE_DIRS})
    endif()

    find_package(SFML REQUIRED system network audio)
    if (SFML_FOUND)
        message(STATUS "Packages: SFML found")
        include_directories(${SFML_INCLUDE_DIRS})
        link_libraries(${SFML_LIBRARIES})
    endif()
elseif(WIN32)
    # Windows, Apple won't be supported
    include_directories(${devto_gl_SOURCE_DIR}/Windows/include)

    find_library(GLFW_LIB       NAMES "glfw3.lib"      PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-glfw/lib-vc2015)
    find_library(FLAC_LIB       NAMES "flac.lib"       PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(OGG_LIB        NAMES "ogg.lib"        PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(OPENAL32_LIB   NAMES "openal32.lib"   PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(VORBIS_LIB     NAMES "vorbis.lib"     PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(VORBISENC_LIB  NAMES "vorbisenc.lib"  PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(VORBISFILE_LIB NAMES "vorbisfile.lib" PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    
    find_library(SFML_AUDIO_LIB   NAMES "sfml-audio.lib"   PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(SFML_NETWORK_LIB NAMES "sfml-network.lib" PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(SFML_SYSTEM_LIB  NAMES "sfml-system.lib"  PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    
    find_library(SFML_AUDIO_LIB_DEBUG   NAMES "sfml-audio-d.lib"   PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(SFML_NETWORK_LIB_DEBUG NAMES "sfml-network-d.lib" PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
    find_library(SFML_SYSTEM_LIB_DEBUG  NAMES "sfml-system-d.lib"  PATHS ${devto_gl_SOURCE_DIR}/Windows/libs/lib-sfml)
else()
    message(AUTHOR_WARNING "Operating System not supported")
endif()

# adding game include directories and source files

include_directories(
    ${devto_gl_SOURCE_DIR}/include
    ${devto_gl_SOURCE_DIR}/extern
    ${devto_gl_SOURCE_DIR}/extern/lua-5-3-4
    ${devto_gl_SOURCE_DIR}/submodules/Zavtrak/include
)

file(GLOB_RECURSE SOURCE_FILES
    # Zavtrak, 3D rendering engine
    ${devto_gl_SOURCE_DIR}/submodules/Zavtrak/src/Zavtrak/*.cpp
    ${devto_gl_SOURCE_DIR}/submodules/Zavtrak/src/glad/*.c
    ${devto_gl_SOURCE_DIR}/submodules/Zavtrak/src/stb/*.cpp

    # imgui, bloat-free graphical user interface library
    ${devto_gl_SOURCE_DIR}/extern/imgui/*.cpp

    # lua 5.3.4
    ${devto_gl_SOURCE_DIR}/extern/lua-5-3-4/*.c
    
    # game sources
    ${devto_gl_SOURCE_DIR}/src/*.cpp
)

add_executable(devto_gl ${SOURCE_FILES})

if (WIN32)
    target_link_libraries(
        devto_gl
        ${GLFW_LIB}
        ${FLAC_LIB}
        ${OGG_LIB}
        ${OPENAL32_LIB}
        ${VORBIS_LIB}
        ${VORBISENC_LIB}
        ${VORBISFILE_LIB}
    )
    target_link_libraries(
        devto_gl
        debug
            ${SFML_AUDIO_LIB_DEBUG}
        optimized
            ${SFML_AUDIO_LIB}
    )
    target_link_libraries(
        devto_gl
        debug
            ${SFML_NETWORK_LIB_DEBUG}
        optimized
            ${SFML_NETWORK_LIB}
    )
    target_link_libraries(
        devto_gl
        debug
            ${SFML_SYSTEM_LIB_DEBUG}
        optimized
            ${SFML_SYSTEM_LIB}
    )
endif()

set_target_properties(
    devto_gl
    PROPERTIES
        CXX_STANDARD 17
        CXX_STANDARD_REQUIRED ON
        CXX_EXTENSIONS OFF
)

if (DEBUG MATCHES "true")
    message(STATUS "Debug mode")

    if (CMAKE_COMPILER_IS_GNUCXX)
        set(CMAKE_CXX_FLAGS "-g -DIMGUI_IMPL_OPENGL_LOADER_GLAD")
        #set(CMAKE_CXX_FLAGS "-pg -g -no-pie -DIMGUI_IMPL_OPENGL_LOADER_GLAD")
        #set(CMAKE_EXE_LINKER_FLAGS "-pg")
    endif()
else()
    message(STATUS "Release mode")
endif()