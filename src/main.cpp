#include <Zavtrak/system.hpp>
#include <Zavtrak/common.hpp>
#include <Zavtrak/objects.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <string>

enum class BlockFace {
    Top, Bottom, Front, Back, Right, Left
};

struct Mesh {
    std::vector<float> vertices, normals, texcoords;

    void update(const Mesh& other, std::size_t x, std::size_t y, std::size_t z) {
        for (std::size_t i=0; i < other.vertices.size() / 3; ++i)
        {
            vertices.push_back(other.vertices[    i * 3] + x);
            vertices.push_back(other.vertices[1 + i * 3] + y);
            vertices.push_back(other.vertices[2 + i * 3] + z);
        }
        normals.insert(normals.end(), other.normals.begin(), other.normals.end());
        texcoords.insert(texcoords.end(), other.texcoords.begin(), other.texcoords.end());
    }

    std::size_t triangleCount()
    {
        return vertices.size() / 3;
    }
};

inline Mesh getMeshFace(BlockFace face)
{
    if (face == BlockFace::Top)
        return Mesh {
            {
                -0.5f,  0.5f, -0.5f,
                 0.5f,  0.5f, -0.5f,
                 0.5f,  0.5f,  0.5f,
                 0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f, -0.5f
            },
            {
                0.0f,  1.0f,  0.0f
            },
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,
                0.0f, 0.0f
            }
        };
    else if (face == BlockFace::Bottom)
        return Mesh {
            {
                -0.5f, -0.5f, -0.5f,
                 0.5f, -0.5f, -0.5f,
                 0.5f, -0.5f,  0.5f,
                 0.5f, -0.5f,  0.5f,
                -0.5f, -0.5f,  0.5f,
                -0.5f, -0.5f, -0.5f
            },
            {
                0.0f, -1.0f,  0.0f
            },
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,
                0.0f, 0.0f
            }
        };
    else if (face == BlockFace::Front)
        return Mesh {
            {
                -0.5f, -0.5f,  0.5f,
                 0.5f, -0.5f,  0.5f,
                 0.5f,  0.5f,  0.5f,
                 0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f,
                -0.5f, -0.5f,  0.5f
            },
            {
                0.0f,  0.0f,  1.0f
            },
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,
                0.0f, 0.0f
            }
        };
    else if (face == BlockFace::Back)
        return Mesh {
            {
                -0.5f, -0.5f, -0.5f,
                 0.5f, -0.5f, -0.5f,
                 0.5f,  0.5f, -0.5f,
                 0.5f,  0.5f, -0.5f,
                -0.5f,  0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f
            },
            {
                0.0f,  0.0f, -1.0f
            },
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,
                0.0f, 0.0f
            }
        };
    else if (face == BlockFace::Right)
        return Mesh {
            {
                0.5f,  0.5f,  0.5f,
                0.5f,  0.5f, -0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f, -0.5f,  0.5f,
                0.5f,  0.5f,  0.5f
            },
            {
                1.0f,  0.0f,  0.0f
            },
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,
                0.0f, 0.0f
            }
        };
    else  // face == BlockFace::Left
        return Mesh {
            {
                -0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f,
                -0.5f, -0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f
            },
            {
                -1.0f,  0.0f,  0.0f
            },
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,
                0.0f, 0.0f
            }
        };
}

namespace Chunk {
    std::size_t Width = 16, Height = 16, Depth = 16;
}

void puts(const std::string& s)
{
    std::cout << s << std::endl;
}

int main(int argc, char** argv)
{
    auto window = zk::system::Window(800, 600, "Zavtrak");
    auto& keyboard_ref = window.getKeyboard();
    auto& mouse_ref = window.getMouse();
    auto clock = zk::system::Clock();
    auto shader = zk::Shader("shaders/3d/shader.vert", "shaders/3d/shader.frag");
    auto texture = zk::Texture("images/container.jpg");

    zk::system::Camera camera;
    camera.move(glm::vec3(0, 0, 48));
    camera.updateVectors();

    double ox = window.getWidth() / 2.0,
           oy = window.getHeight() / 2.0;
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), window.getAspectRatio(), 0.1f, 100.0f);

    // controls
    {
        keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::W, [&camera, &shader] () {
            camera.processKeyboard(zk::system::CameraMovement::Forward, 1.0f);
            shader.setMat4("view", camera.getViewMatrix());

            camera.updateVectors();
        });
        keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::S, [&camera, &shader] () {
            camera.processKeyboard(zk::system::CameraMovement::Backward, 1.0f);
            shader.setMat4("view", camera.getViewMatrix());

            camera.updateVectors();
        });
        keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::A, [&camera, &shader] () {
            camera.processKeyboard(zk::system::CameraMovement::Left, 1.0f);
            shader.setMat4("view", camera.getViewMatrix());

            camera.updateVectors();
        });
        keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::D, [&camera, &shader] () {
            camera.processKeyboard(zk::system::CameraMovement::Right, 1.0f);
            shader.setMat4("view", camera.getViewMatrix());

            camera.updateVectors();
        });

        keyboard_ref.addCallback(zk::EventType::Pressed, zk::Key::Escape, [&window] () { window.close(); });

        // when mouse is being captured, move view around
        mouse_ref.setMouseMoveCallback([&camera, &ox, &oy, &shader] (double x, double y) {
            camera.processMouseMovement(x - ox, oy - y);
            camera.updateVectors();
            shader.setMat4("view", camera.getViewMatrix());
            ox = x; oy = y;
        });

        window.updateCallbacks();
    }

    bool blocks[Chunk::Width][Chunk::Height][Chunk::Depth];
    // "map generation"
    for (int x=0; x < Chunk::Width; ++x)
    {
        for (int y=0; y < Chunk::Height; ++y)
        {
            for (int z=0; z < Chunk::Depth; ++z)
            {
                // putting a block everywhere (nearly)

                if (y == 10)
                    blocks[x][y][z] = false;
                else if (z > 12)
                    blocks[x][y][z] = false;
                else
                    blocks[x][y][z] = true;
            }
        }
    }
    
    // creating mesh
    Mesh temp_mesh;
    for (int x=0; x < Chunk::Width; ++x)
    {
        for (int y=0; y < Chunk::Height; ++y)
        {
            for (int z=0; z < Chunk::Depth; ++z)
            {
                if (x > 0 && blocks[x-1][y][z] == false)
                {
                    // add face back
                    temp_mesh.update(getMeshFace(BlockFace::Back), x-1, y, z);
                }
                if (y > 0 && blocks[x][y-1][z] == false)
                {
                    // add face bottom
                    temp_mesh.update(getMeshFace(BlockFace::Bottom), x, y-1, z);
                }
                if (z > 0 && blocks[x][y][z-1] == false)
                {
                    // add face left
                    temp_mesh.update(getMeshFace(BlockFace::Left), x, y, z-1);
                }
                if (x + 1 < Chunk::Width && blocks[x+1][y][z] == false)
                {
                    // add face front
                    temp_mesh.update(getMeshFace(BlockFace::Front), x+1, y, z);
                }
                if (y + 1 < Chunk::Height && blocks[x][y+1][z] == false)
                {
                    // add face top
                    temp_mesh.update(getMeshFace(BlockFace::Top), x, y+1, z);
                }
                if (z + 1 < Chunk::Depth && blocks[x][y][z+1] == false)
                {
                    // add face right
                    temp_mesh.update(getMeshFace(BlockFace::Right), x, y, z+1);
                }
            }
        }
    }

    std::cout << temp_mesh.vertices.size() << " vertices" << std::endl;

    // creating vertices
    std::vector<float> temp_vertices;
    for (std::size_t i=0; i < temp_mesh.vertices.size() / 3; ++i)  // we will move from one line to another
    {
        temp_vertices.push_back(temp_mesh.vertices[    i * 3]);
        temp_vertices.push_back(temp_mesh.vertices[1 + i * 3]);
        temp_vertices.push_back(temp_mesh.vertices[2 + i * 3]);

        temp_vertices.push_back(temp_mesh.texcoords[    i * 3]);
        temp_vertices.push_back(temp_mesh.texcoords[1 + i * 3]);
    }
    
    zk::objects::VertexArray VAO;
    zk::objects::VertexBuffer VBO;
    VAO.bind();
    zk::Vertices vertices(temp_vertices);
    VBO.setData(vertices);
    zk::objects::setVertexAttrib(0, 3, 5, 0);
    zk::objects::setVertexAttrib(1, 2, 5, 3);

    shader.use();
    shader.setUniform1<int>("texture1", 0);

    shader.setMat4("projection", projection);
    shader.setMat4("view", camera.getViewMatrix());

    while(!window.shouldClose())
    {
        // delta time handling
        clock.tick();
        double dt = clock.timeSinceLastTick();
        std::cout << "dt: " << dt << " - FPS: " << 1 / dt << "\r";

        // events handling
        window.pollEvents();
        
        // rendering
        // first, clear screen
        window.clear(zk::Color::Cyan);

        // then render the container
        texture.bind(0);
        shader.use();

        // render container
        VAO.bind();
        // create transformations
        glm::mat4 model = glm::mat4(1.0f);
        // pass it to the shader
        shader.setMat4("model", model);
        glDrawArrays(GL_TRIANGLES, 0, temp_mesh.triangleCount());

        // swap buffers
        window.display();
    }

    return 0;
}
